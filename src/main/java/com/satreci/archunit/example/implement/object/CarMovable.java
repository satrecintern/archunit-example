package com.satreci.archunit.example.implement.object;

import com.satreci.archunit.example.implement.Car;
import com.satreci.archunit.example.implement.Movable;

public class CarMovable extends Car implements Movable {
}
