package com.satreci.archunit.example.onion.adapter.interfaces;

import com.satreci.archunit.example.onion.application.ApplicationB;
import com.satreci.archunit.example.onion.domain.service.ServiceB;

public class InterfaceB {
    ApplicationB appB;
    ServiceB serviceB;
}
