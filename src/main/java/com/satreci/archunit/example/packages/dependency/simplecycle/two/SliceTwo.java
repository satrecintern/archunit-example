package com.satreci.archunit.example.packages.dependency.simplecycle.two;

import com.satreci.archunit.example.packages.dependency.simplecycle.three.SliceThree;

public class SliceTwo {
    SliceThree sliceThree;
    public SliceTwo(){
        sliceThree = new SliceThree();
    }
}
