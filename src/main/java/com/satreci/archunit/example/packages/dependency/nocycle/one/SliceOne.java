package com.satreci.archunit.example.packages.dependency.nocycle.one;

import com.satreci.archunit.example.packages.dependency.nocycle.three.SliceTwo;

public class SliceOne {
    SliceTwo sliceTwo;

    public SliceOne(){
        sliceTwo = new SliceTwo();
    }
}
