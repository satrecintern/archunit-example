package com.satreci.archunit.example.packages.dependency.simplecycle.one;

import com.satreci.archunit.example.packages.dependency.simplecycle.two.SliceTwo;

public class SliceOne {
    SliceTwo sliceTwo;
    public SliceOne(){
        sliceTwo = new SliceTwo();
    }
}
