package com.satreci.archunit.example.packages.dependency.simplecycle.three;

import com.satreci.archunit.example.packages.dependency.simplecycle.one.SliceOne;

public class SliceThree {
    SliceOne sliceOne;
    public SliceThree(){
        sliceOne = new SliceOne();
    }
}
