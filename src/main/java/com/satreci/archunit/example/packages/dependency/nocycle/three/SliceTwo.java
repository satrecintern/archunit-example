package com.satreci.archunit.example.packages.dependency.nocycle.three;

import com.satreci.archunit.example.packages.dependency.nocycle.two.SliceThree;

public class SliceTwo {
    SliceThree sliceThree;
    public SliceTwo(){
        sliceThree = new SliceThree();
    }
}
