package com.satreci.archunit.example;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import static com.tngtech.archunit.library.Architectures.layeredArchitecture;

import org.junit.Test;


public class LayeredArchitectureTest {
    private final JavaClasses classes = new ClassFileImporter().importPackages("com.satreci.archunit.example.tier3");

    @Test
    public void tier3_check(){
        layeredArchitecture()
                .layer("Controller").definedBy("..controller..")
                .layer("Service").definedBy("..service..")
                .layer("Persistance").definedBy("..persistance..")

                .whereLayer("Controller").mayNotBeAccessedByAnyLayer()
                .whereLayer("Service").mayOnlyBeAccessedByLayers("Controller")
                .whereLayer("Persistance").mayOnlyBeAccessedByLayers("Service")

                .check(classes);
    }
}