package com.satreci.archunit.example;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import org.junit.Ignore;
import org.junit.Test;

import static com.tngtech.archunit.library.dependencies.SlicesRuleDefinition.slices;

public class CycleTest {
    private final JavaClasses classes = new ClassFileImporter().importPackages("com.satreci.archunit.example.packages.dependency");

    @Ignore
    public void slices_in_simplecycle_should_be_free_of_cycles(){
        slices().matching("com.satreci.archunit.example.packages.dependency.simplecycle.(*)..")
                .should().beFreeOfCycles()
                .check(classes);
    }

    @Test
    public void slices_in_nocycle_should_be_free_of_cycles(){
        slices().matching("com.satreci.archunit.example.packages.dependency.nocycle.(*)..")
                .should().beFreeOfCycles()
                .check(classes);
    }

    @Test
    public void slices_in_independent_should_no_depend_on_each_other(){
        slices().matching("com.satreci.archunit.example.packages.dependency.independent.(*)..")
                .should().notDependOnEachOther()
                .check(classes);
    }
}
