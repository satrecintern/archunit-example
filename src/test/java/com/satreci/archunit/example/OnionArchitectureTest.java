package com.satreci.archunit.example;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import org.junit.Test;

import static com.tngtech.archunit.library.Architectures.onionArchitecture;


public class OnionArchitectureTest{
    private final JavaClasses classes = new ClassFileImporter().importPackages("com.satreci.archunit.example.onion");

    @Test
    public void onion_check(){

        onionArchitecture()
                .domainModels("..domain.model..")
                .domainServices("..domain.service..")
                .applicationServices("..application..")
                .adapter("infrastructure","..infrastructure..")
                .adapter("interfaces","..interfaces..")
                .adapter("test","..test..")
                .check(classes);
    }
}