package com.satreci.archunit.example;

import com.satreci.archunit.example.annotation.foobar.Foo;
import com.satreci.archunit.example.annotation.fruits.Fruit;
import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import org.junit.Test;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.classes;

public class AnnotationTest {

    private final JavaClasses classes = new ClassFileImporter().importPackages("com.satreci.archunit.example.annotation");

    @Test
    public void bar_should_be_annotated_with_foo() {
        classes()
                .that().haveSimpleName("Bar")
                .should().beAnnotatedWith(Foo.class)
                .check(classes);
    }

    @Test
    public void apple_should_be_annotated_with_fruit() {
        classes()
                .that().haveSimpleName("Apple")
                .should().beAnnotatedWith(Fruit.class)
                .check(classes);
    }

    @Test
    public void banana_should_be_annotated_with_fruit() {
        classes()
                .that().haveSimpleName("Banana")
                .should().beAnnotatedWith("Fruit")
                .check(classes);
    }
}
