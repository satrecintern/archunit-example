package com.satreci.archunit.example;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import org.junit.Test;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.classes;
import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

public class NameTest {
    private final JavaClasses classes = new ClassFileImporter().importPackages("com.satreci.archunit.example.classname");

    @Test
    public void have_simple_name_starting_with_bar_should_reside_in_a_package_bar(){
        classes()
                .that().haveSimpleNameStartingWith("bar")
                .should().resideInAPackage("..bar..")
                .check(classes);
    }

    @Test
    public void reside_in_a_package_bar_should_have_simple_name_starting_with_bar(){
        classes()
                .that().resideInAPackage("..bar..")
                .should().haveSimpleNameStartingWith("bar")
                .check(classes);
    }

    @Test
    public void no_classes_reside_in_any_package_foo_should_have_simple_name_starting_with_bar(){
        noClasses()
                .that().resideInAnyPackage("..foo..")
                .should().haveSimpleNameStartingWith("bar")
                .check(classes);
    }

    @Test
    public void have_simple_name_ending_with_test_should_reside_in_any_package_test(){
        noClasses()
                .that().haveSimpleNameEndingWith("test")
                .should().resideInAPackage("..test..")
                .check(classes);
    }
}
