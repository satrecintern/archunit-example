package com.satreci.archunit.example;

import com.satreci.archunit.example.implement.Car;
import com.satreci.archunit.example.implement.Movable;
import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import org.junit.Test;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.classes;

public class InheritanceTest {
    private final JavaClasses classes = new ClassFileImporter().importPackages("com.satreci.archunit.example.implement.object");

    @Test
    public void have_simple_name_starting_with_car_should_be_assignable_to_car(){
        classes()
                .that().haveSimpleNameStartingWith("Car")
                .should().beAssignableTo(Car.class)
                .check(classes);
    }

    @Test
    public void have_simple_name_ending_with_movable_should_implement_movable(){
        classes()
                .that().haveSimpleNameEndingWith("Movable")
                .should().implement(Movable.class)
                .check(classes);
    }
}
