package com.satreci.archunit.example;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.domain.JavaModifier;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.classes;

import org.junit.Test;


public class ModifierTest{
    private final JavaClasses classes = new ClassFileImporter().importPackages("com.satreci.archunit.example.modifier");

    @Test
    public void have_simple_name_foo_should_have_modifier_public(){
        classes()
                .that().haveSimpleName("foo")
                .should().haveModifier(JavaModifier.PUBLIC)
                .check(classes);
    }

    @Test
    public void have_simple_name_bar_should_have_modifier_public(){
        classes()
                .that().haveSimpleName("bar")
                .should().haveModifier(JavaModifier.PUBLIC)
                .check(classes);
    }

    @Test
    public void have_simple_name_foobar_should_have_modifier_private(){
        classes()
                .that().haveSimpleName("foobar")
                .should().haveModifier(JavaModifier.PRIVATE)
                .check(classes);
    }

    @Test
    public void no_classes_have_simple_name_containing_foo_should_have_modifier_public(){
        classes()
                .that().haveSimpleNameStartingWith("foo")
                .should().haveModifier(JavaModifier.PUBLIC)
                .check(classes);
    }
}