package com.satreci.archunit.example;

import com.tngtech.archunit.base.DescribedPredicate;
import com.tngtech.archunit.core.domain.JavaClass;
import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.domain.JavaMethod;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.lang.ArchCondition;
import com.tngtech.archunit.lang.ConditionEvents;
import com.tngtech.archunit.lang.SimpleConditionEvent;
import org.junit.Test;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.classes;

public class CustomTest {
    private final JavaClasses classes = new ClassFileImporter().importPackages("com.satreci.archunit.example.custom");

    @Test
    public void annotated_with_test_should_have_name_starting_with_test(){
        classes()
                .that(new DescribedPredicate<JavaClass>("annotated with TEST") {
                    @Override
                    public boolean apply(JavaClass input) {
                        return input.isAnnotatedWith(com.satreci.archunit.example.custom.TEST.class);
                    }
                })
                .should().haveSimpleNameStartingWith("Test")
                .check(classes);
    }

    @Test
    public void annotated_with_payload_should_have_method_(){
        DescribedPredicate<JavaClass> annotatedWithPayload =
                new DescribedPredicate<JavaClass>("annotated with @Payload"){
                    @Override
                    public boolean apply(JavaClass input) {
                        return input.isAnnotatedWith("Payload");
                    }
                };

        ArchCondition<JavaClass> haveMethodThatHaveName_foo = new ArchCondition<JavaClass>("have foobar method") {
            @Override
            public void check(JavaClass item, ConditionEvents events) {
                boolean hasMethod = false;
                for(JavaMethod method : item.getMethods()){
                    if(method.getName().equals("foo")) {
                        hasMethod = true;
                        break;
                    }
                }
                if(!hasMethod) {
                    String msg = String.format("Method testtest does not exist in %s",item.getName());
                    events.add(SimpleConditionEvent.violated(item, msg));
                }
            }
        };

        classes()
                .that(annotatedWithPayload)
                .should(haveMethodThatHaveName_foo).check(classes);
    }
}
