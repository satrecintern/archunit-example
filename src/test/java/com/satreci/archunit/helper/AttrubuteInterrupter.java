package com.satreci.archunit.helper;

import com.tngtech.archunit.lang.ArchCondition;
import com.tngtech.archunit.lang.ConditionEvents;

import java.util.ArrayList;
import java.util.List;

public class AttrubuteInterrupter<T> extends ArchCondition<T> {

    public List<String> classList = new ArrayList<String>();
    public IAttributeInterrupter<T> attrubuteInterrupter;

    public AttrubuteInterrupter(String description, Object... args) {
        super(description, args);
    }

    public AttrubuteInterrupter(String description, IAttributeInterrupter<T> callback, Object... args) {
        this(description, args);
        this.attrubuteInterrupter = callback;
    }

    @Override
    public void check(T item, ConditionEvents events) {
        classList.add(attrubuteInterrupter.get(item));
    }

    public interface IAttributeInterrupter<T> {
        public String get(T item);
    }

}